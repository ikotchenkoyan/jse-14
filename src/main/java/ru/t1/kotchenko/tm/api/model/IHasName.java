package ru.t1.kotchenko.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
