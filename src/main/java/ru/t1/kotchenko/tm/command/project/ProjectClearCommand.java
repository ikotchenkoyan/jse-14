package ru.t1.kotchenko.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectTaskService().removeProjects();
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

}
