package ru.t1.kotchenko.tm.command.project;

import ru.t1.kotchenko.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public String getName() {
        return "project-create";
    }

}
