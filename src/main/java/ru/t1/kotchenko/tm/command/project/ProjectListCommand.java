package ru.t1.kotchenko.tm.command.project;

import ru.t1.kotchenko.tm.enumerated.Sort;
import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.model.Project;
import ru.t1.kotchenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(sort);
        renderProjects(projects);
    }

    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public String getName() {
        return "project-list";
    }

    protected void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            final String name = project.getName();
            final String description = project.getDescription();
            final String status = Status.toName(project.getStatus());
            System.out.printf("%s. %s : %s : %s \n", index, name, description, status);
            index++;
        }
    }

}
