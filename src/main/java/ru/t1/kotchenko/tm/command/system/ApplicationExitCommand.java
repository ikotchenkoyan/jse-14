package ru.t1.kotchenko.tm.command.system;

public class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getArgument() {
        return "";
    }

    @Override
    public String getDescription() {
        return "Close application.";
    }

    @Override
    public String getName() {
        return "exit";
    }

}
