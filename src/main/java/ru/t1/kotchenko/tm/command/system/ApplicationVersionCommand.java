package ru.t1.kotchenko.tm.command.system;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.14.3");
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show application version.";
    }

    @Override
    public String getName() {
        return "version";
    }

}
