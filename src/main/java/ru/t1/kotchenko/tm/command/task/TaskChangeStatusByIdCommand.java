package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusName);
        getTaskService().changeTaskStatusById(id, status);
    }

    @Override
    public String getDescription() {
        return "Change task status by id.";
    }

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

}
