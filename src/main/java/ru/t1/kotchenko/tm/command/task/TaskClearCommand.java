package ru.t1.kotchenko.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        getTaskService().deleteAll();
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

}
