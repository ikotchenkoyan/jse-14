package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.enumerated.Sort;
import ru.t1.kotchenko.tm.model.Task;
import ru.t1.kotchenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return "Show task list.";
    }

    @Override
    public String getName() {
        return "task-list";
    }

}
