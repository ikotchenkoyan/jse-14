package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        getTaskService().updateByIndex(index, name, description);
    }

    @Override
    public String getDescription() {
        return "Update task by index.";
    }

    @Override
    public String getName() {
        return "task-update-by-index";
    }

}
