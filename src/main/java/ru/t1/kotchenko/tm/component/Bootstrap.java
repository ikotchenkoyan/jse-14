package ru.t1.kotchenko.tm.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.kotchenko.tm.api.component.IBootstrap;
import ru.t1.kotchenko.tm.api.repository.ICommandRepository;
import ru.t1.kotchenko.tm.api.repository.IProjectRepository;
import ru.t1.kotchenko.tm.api.repository.ITaskRepository;
import ru.t1.kotchenko.tm.api.service.*;
import ru.t1.kotchenko.tm.command.AbstractCommand;
import ru.t1.kotchenko.tm.command.project.*;
import ru.t1.kotchenko.tm.command.system.*;
import ru.t1.kotchenko.tm.command.task.*;
import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kotchenko.tm.exception.system.CommandNotSupportedException;
import ru.t1.kotchenko.tm.repository.CommandRepository;
import ru.t1.kotchenko.tm.repository.ProjectRepository;
import ru.t1.kotchenko.tm.repository.TaskRepository;
import ru.t1.kotchenko.tm.service.CommandService;
import ru.t1.kotchenko.tm.service.ProjectService;
import ru.t1.kotchenko.tm.service.ProjectTaskService;
import ru.t1.kotchenko.tm.service.TaskService;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator, IBootstrap {

    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    {
        registry(new ApplicationDeveloperCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindToProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public void run(final String... args) {
        if (processArguments(args)) exit();
        initDemoData();
        initLogger();
        parseCommands();
    }

    private void initDemoData() {
        projectService.create("TEST PROJECT", Status.IN_PROGRESS);
        projectService.create("DEMO PROJECT", Status.NOT_STARTED);
        projectService.create("ALPHA PROJECT", Status.IN_PROGRESS);
        projectService.create("BETTA PROJECT", Status.COMPLETED);

        taskService.create("GAMMA TASK", Status.NOT_STARTED);
        taskService.create("DELTA TASK", Status.NOT_STARTED);
    }

    private void parseCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                processCommand(command);
                System.out.println("OK");
            } catch (Exception e) {
                LOGGER_LIFECYCLE.error("Invalid terminal command.");
                System.out.println("FAIL");
            }
        }
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***");
            }
        });
    }

    public void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    public boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

}
