package ru.t1.kotchenko.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityExceotion {

    public TaskNotFoundException() {
        super("Error! Task not found.");
    }

}
