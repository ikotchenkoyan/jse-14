package ru.t1.kotchenko.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldExceotion {

    public DescriptionEmptyException() {
        super("Error! Description is empty.");
    }

}
