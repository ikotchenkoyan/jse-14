package ru.t1.kotchenko.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldExceotion {

    public NumberIncorrectException() {
        super("Error! Number is incorrect.");
    }

    public NumberIncorrectException(final String value, Throwable cause) {
        super("Error! This value \"" + value + " is incorrect.", cause);
    }

}
